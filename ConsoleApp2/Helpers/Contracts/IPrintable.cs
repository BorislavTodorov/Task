﻿using ConsoleApp2.Models;

namespace ConsoleApp2.Helpers.Contracts
{
    public interface IPrintable
    {
        void PrintData(Purchase purchase);

        void ErrorMessage(string cardType);
    }
}