﻿using ConsoleApp2.Cards.Contracts;

namespace ConsoleApp2.Cards
{
    public class CardFactory : ICardFactory
    {
        public ICard ProcessCards(string cardType)
        {
            switch (cardType)
            {
                case "Bronze":
                    return new BronzeCard();
                case "Silver":
                    return new SilverCard();
                case "Gold":
                    return new GoldCard();
                default:
                    return null;
            }
        }
    }
}