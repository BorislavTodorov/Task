﻿namespace ConsoleApp2.Models
{
    public class Purchase
    {
        public double Value { get; set; }

        public string Card { get; set; }

        public double DiscountRate { get; set; }

        public double Discount { get; set; }

        public double Total { get; set; }
    }
}