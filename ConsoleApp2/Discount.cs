﻿using ConsoleApp2.Cards;
using ConsoleApp2.Cards.Contracts;
using ConsoleApp2.Helpers;
using ConsoleApp2.Helpers.Contracts;
using ConsoleApp2.Models;

namespace ConsoleApp2
{
    public class Discount
    {
        private readonly ICardFactory cardFactory;
        private readonly IPrintable print;

        public Discount()
        {
            this.print = new Print();
            this.cardFactory = new CardFactory();
        }

        public void Process(double valueOfPurchase, double turnover, string cardType)
        {
            var card = this.cardFactory.ProcessCards(cardType);

            if (card != null)
            {
                var discountRate = card.GetDiscountRate(turnover);
                var discount = this.GetDiscount(valueOfPurchase, discountRate);
                var total = this.GetTotal(valueOfPurchase, discount);
                var purchase = new Purchase { Card = cardType, Discount = discount, DiscountRate = discountRate, Total = total, Value = valueOfPurchase };

                this.print.PrintData(purchase);
            }
            else
            {
                this.print.ErrorMessage(cardType);
            }
           
        }

        private double GetDiscount(double valueOfPurchase, double discountRate)
        {
            var discount = valueOfPurchase * discountRate / 100;

            return discount;
        }

        private double GetTotal(double valueOfPurchase, double discount)
        {
            var total = valueOfPurchase - discount;

            return total;
        }
    }
}