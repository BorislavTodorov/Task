﻿using ConsoleApp2.Cards.Contracts;

namespace ConsoleApp2.Cards
{
    public class BronzeCard : ICard
    {
        public double GetDiscountRate(double turnover)
        {
            double discountRate = 0;

            if (turnover >= 100 && turnover <= 300)
            {
                discountRate = 1;
            }

            if (turnover > 300)
            {
                discountRate = 2.5;
            }

            return discountRate;
        }
    }
}