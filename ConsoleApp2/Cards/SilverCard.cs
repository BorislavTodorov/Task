﻿using ConsoleApp2.Cards.Contracts;

namespace ConsoleApp2.Cards
{
    public class SilverCard : ICard
    {
        public double GetDiscountRate(double turnover)
        {
            double discountRate = 2;

            if (turnover > 300)
            {
                discountRate = 3.5;
            }

            return discountRate;
        }
    }
}