﻿namespace ConsoleApp2.Cards.Contracts
{
    public interface ICardFactory
    {
        ICard ProcessCards(string cardType);
    }
}