﻿using ConsoleApp2.Cards.Contracts;

namespace ConsoleApp2.Cards
{
    public class GoldCard : ICard
    {
        public double GetDiscountRate(double turnover)
        {
            double discountRate = 2;

            if (turnover > 100)
            {
                discountRate += turnover / 100;

                if (discountRate > 10)
                {
                    discountRate = 10;
                }
            }

            return discountRate;
        }
    }
}