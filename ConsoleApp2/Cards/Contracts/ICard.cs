﻿namespace ConsoleApp2.Cards.Contracts
{
    public interface ICard
    {
        double GetDiscountRate(double turnover);
    }
}