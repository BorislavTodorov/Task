﻿using System;
using ConsoleApp2.Helpers.Contracts;
using ConsoleApp2.Models;

namespace ConsoleApp2.Helpers
{
    public class Print : IPrintable
    {
        public void PrintData(Purchase purchase)
        {
            Console.WriteLine($"{purchase.Card}:");
            Console.WriteLine($"Purchase value: ${purchase.Value}");
            Console.WriteLine($"Discount rate: {purchase.DiscountRate}%");
            Console.WriteLine($"Discount: ${purchase.Discount}");
            Console.WriteLine($"Total: ${purchase.Total}");
        }

        public void ErrorMessage(string cardType)
        {
            Console.WriteLine($"Type of {cardType} not supported. Please use one of the supported card types: Bronze, Silver or Gold");
        }
    }
}